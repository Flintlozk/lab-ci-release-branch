#!/bin/bash
if [ $# -gt 0 ] 
then
    echo $branch_name
    . ./tag.sh
    if [ $exit_code -ne 0 ] ; then
        RED='\033[0;31m'
        NC='\033[1;33m' # No Color
        echo -e "${RED}ERROR${NC} Please GIT Commit before publish"      
    elif [ $1 = "water" ]
    then
    echo "* * * Water Publish * * *"
        git add --all
        RELEASE_BRANCH=release/Water/$PACKAGE_VERSION
        git checkout -b $RELEASE_BRANCH
        git commit -m "Build $RELEASE_BRANCH"
        git push origin $RELEASE_BRANCH
        git checkout develop
        git pull origin $RELEASE_BRANCH
    elif [ $1 = "fire" ]
    then
    echo "* * * Fire Publish * * *"
        git add --all
        RELEASE_BRANCH=release/Fire/$PACKAGE_VERSION
        git checkout -b $RELEASE_BRANCH
        git commit -m "Build $RELEASE_BRANCH"
        git push origin $RELEASE_BRANCH
        git checkout develop
        git pull origin $RELEASE_BRANCH
    else
        echo "Publish Water Project ::: Use water";
        echo "Publish Fire Project ::: Use fire";
    fi
    else
    echo "Publish Water Project ::: Use water";
    echo "Publish Fire Project ::: Use fire";
fi 
